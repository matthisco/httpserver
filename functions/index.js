// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions
let functions = require("firebase-functions");
const fetch = require("node-fetch");

let moment = require("moment");
// exports.getUserDetails = async (req, res) => {
// 	let { google } = require("googleapis");
// 	l
// 	let privatekey = require("./config.json");
// 	let axios = require("axios");
//
// 	let jwt = new google.auth.JWT(
// 		privatekey.client_email,
// 		null,
// 		privatekey.private_key,
// 		["https://www.googleapis.com/auth/calendar"]
// 	);
//
// 	let ids = [];
// 	let promises = [];
//
// 	function revisedRandId() {
// 		return Math.random()
// 			.toString(36)
// 			.replace(/[^a-z]+/g, "")
// 			.substr(2, 10);
// 	}
//
// 	const token = await jwt.authorize();
//
// 	let headers = {
// 		"Access-Control-Allow-Origin": "*",
// 		"Content-Type": "application/json;charset=UTF-8",
// 		Authorization: token.token_type + " " + token.access_token
// 	};
//
// 	axios
// 		.post(
// 			"https://www.googleapis.com/calendar/v3/calendars/thirdyear@rguc.co.uk/events",
// 			{ headers }
// 		)
// 		.then(function(response) {
// 			console.log(response);
// 		})
// 		.catch(function(error) {
// 			console.log(error);
// 		});
// };

async function main() {
	let { google } = require("googleapis");

	let privatekey = require("./config.json");
	let axios = require("axios");

	let jwt = new google.auth.JWT(
		privatekey.client_email,
		null,
		privatekey.private_key,
		["https://www.googleapis.com/auth/calendar"]
	);

	const start = moment().format("YYYY-MM-DD") + "T" + "07:00:00.000Z"; // set to 23:59 pm today
	const end = moment().format("YYYY-MM-DD") + "T" + "20:00:00.000Z"; // set to 23:59 pm today

	const token = await jwt.authorize();
	let headers = {
		"Access-Control-Allow-Origin": "*",
		"Content-Type": "application/json;charset=UTF-8",
		Authorization: token.token_type + " " + token.access_token
	};

	let data = {
		singleEvents: true
	};

	axios
		.get(
			`https://www.googleapis.com/calendar/v3/calendars/thirdyear@rguc.co.uk/events?singleEvents=true&orderBy=startTime&timeMax=${end}&timeMin=${start}&key=${
				privatekey.key
			}`,
			{ headers }
		)
		.then(function(response) {
			const changed = response.data.items.filter(
				event => event.updated <= end && event.updated >= start
			);

			console.log(changed);
		})
		.catch(function(error) {
			console.log(error.response.data.error);
		});
}

main();

exports.helloWorld = functions.https.onRequest((request, response) => {
	main();
});

// exports.helloAsync = async data => {
//   const result = await fetch('https://www.example.com');
//   return result;
// };
